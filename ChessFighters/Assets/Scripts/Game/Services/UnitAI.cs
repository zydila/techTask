﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Core;
using Game.Models;
using Game.Services;
using UnityEngine;

namespace Game.Services
{
    //TODO: unit logic here, including pathFinding. I used simple pathFinding, searching for closest attacking position and then closest next tile(cell) to attacking position
    public class UnitAI : IUnitService
    {
        private const float EXECUTION_DELAY = 0.5f;

        private readonly MapModel mapModel = DependencyInjector.Resolve<MapModel>();
        private readonly GameModel gameModel = DependencyInjector.Resolve<GameModel>();
        
        private UnitModel unitModel;
        private float thinkDelayCounter = 10f;
        private Action onComplete;
        private UnitMover mover;
        private UnitAttacker attacker;
        private List<UnitModel> enemyUnits;
        private UnitModel selectedUnitTarget;
        private Tile selectedTile;

        public void Init(UnitModel unit)
        {
            unitModel = unit;
            enemyUnits = gameModel.AllUnits.FindAll(enemyUnit => unitModel.UnitSide != enemyUnit.UnitSide);
        }

        public void SetUnitServices(UnitMover mover, UnitAttacker attacker)
        {
            this.mover = mover;
            this.attacker = attacker;
        }

        public bool Execute(float frameDeltaTime)
        {
            if (enemyUnits == null || enemyUnits.Count == 0 || mover.IsMoving)
            {
                attacker.StopAttacking();
                return false;
            }

            thinkDelayCounter += frameDeltaTime;
            if (thinkDelayCounter < EXECUTION_DELAY)
            {
                return false;
            }

            thinkDelayCounter = 0;
            if (selectedUnitTarget == null || selectedUnitTarget.State == UnitState.Dead)
            {
                selectedUnitTarget = FindClosestEnemy();
            }

            if (selectedUnitTarget == null)
            {
                mover.StopMovement();
                attacker.StopAttacking();
                return false;
            }

            if (Mathf.Abs(selectedUnitTarget.CurrentTile.PositionInGrid.x - unitModel.CurrentTile.PositionInGrid.x) <= 1 &&
                Mathf.Abs(selectedUnitTarget.CurrentTile.PositionInGrid.y - unitModel.CurrentTile.PositionInGrid.y) <= 1)
            {

                mover.StopMovement();
                attacker.SetTarget(selectedUnitTarget);
                attacker.OnComplete(() => thinkDelayCounter = float.MaxValue);
            }
            else
            {
                attacker.StopAttacking();
                selectedUnitTarget = FindClosestEnemy();
                if (selectedUnitTarget == null)
                {
                    mover.StopMovement();
                    return false;
                }
                var attackFromTile = mapModel.FindClosestTileFromToInRange(selectedUnitTarget.CurrentTile, unitModel.CurrentTile, 1, true);
                var tileToMove = mapModel.FindClosestTileFromToInRange(unitModel.CurrentTile, attackFromTile ,1, false);
                mover.SetTargetPosition(tileToMove);
                mover.OnComplete(() => thinkDelayCounter = float.MaxValue);
            }

            return true;
        }

        public void OnComplete(Action doOnComplete)
        {
            onComplete = doOnComplete;
        }

        private UnitModel FindClosestEnemy()
        {
            UpdateAliveEnemies();
            return enemyUnits.Count <= 0
                ? null
                : enemyUnits
                    .OrderByDescending(enemy => -1 * (enemy.Position - unitModel.CurrentTile.Position).magnitude)
                    .ToArray()[0];
        }

        private void UpdateAliveEnemies()
        {
            enemyUnits = enemyUnits.FindAll(unit => unit.State == UnitState.Alive);
        }
    }
}
