﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Data;
using Game.Models;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Services
{
    public class UnitBuilder
    {
        private UnitsData data;

        private List<string> unitIds;
        public List<string> UnitIds
        {
            get { return unitIds ?? (unitIds = data.UnitsDatas.Select(unitData => unitData.UnitId).ToList()); }
        }

        private int numberOfSides;
        
        private string unitId;
        private bool buildRandomUnit;
        private SpawnSide unitSide;
        private bool sideSet;
        private Vector3 unitPosition;
        private bool unitPositionSet;
        
        public void Init()
        {
            data = Resources.Load<UnitsData>("Data/UnitsData");
            numberOfSides = Enum.GetNames(typeof(SpawnSide)).Length;;
        }

        public UnitBuilder SetUnitId(string id)
        {
            if (buildRandomUnit)
            {
                return this;
            }
            unitId = id;
            return this;
        }

        public UnitBuilder SetBuildRandom()
        {
            buildRandomUnit = true;
            var randomIndex = Random.Range(0, UnitIds.Count);
            unitId = UnitIds[randomIndex];
            return this;
        }

        public UnitBuilder SetSide(SpawnSide side)
        {
            unitSide = side;
            sideSet = true;
            return this;
        }

        public UnitBuilder SetPosition(Vector3 position)
        {
            unitPositionSet = true;
            unitPosition = position;
            return this;
        }
        
        public UnitModel Build()
        {
            if (!data.GetUnitData(unitId, out UnitData unitData))
            {
                throw new ArgumentException("incorrect unit Id");
            }

            if (!sideSet)
            {
                unitSide = (SpawnSide) Random.Range(0, numberOfSides);
            }
            var result = new UnitModel(unitData, unitSide);
            if (unitPositionSet)
            {
                result.SetNewPosition(unitPosition);
            }

            return result;
        }

        public void Clear()
        {
            unitId = "";
            buildRandomUnit = false;
            sideSet = false;
            unitPositionSet = false;
        }
    }

}
