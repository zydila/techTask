﻿using System;
using Game.Models;

namespace Game.Services
{
    public interface IUnitService
    {
        void Init(UnitModel unit);
        bool Execute(float frameDeltaTime);
        void OnComplete(Action doOnComplete);
    }

}
