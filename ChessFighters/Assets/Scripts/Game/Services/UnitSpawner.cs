﻿using System.Collections.Generic;
using Game.Core;
using Game.Data;
using Game.Models;
using UnityEngine;

namespace Game.Services
{
    //TODO: On a real project I would use pooling
    public class UnitSpawner
    {
        private MapModel mapModel = DependencyInjector.Resolve<MapModel>();
        readonly UnitBuilder unitBuilder = new UnitBuilder();
        private SpawnData spawnData;

        public void Init()
        {
            spawnData = Resources.Load<SpawnData>("Data/SpawnData");
        }
        
        public List<UnitModel> CreateUnits()
        {
            unitBuilder.Clear();
            unitBuilder.Init();
            var units = new List<UnitModel>();
            var numOfUnits = Random.Range(spawnData.MinMaxUnitsPerSide.x, spawnData.MinMaxUnitsPerSide.y + 1);
            for(var i = 0; i < numOfUnits; i++)
            {
                units.Add(CreateRandomUnit(SpawnSide.Left, spawnData));
            }

            numOfUnits = Random.Range(spawnData.MinMaxUnitsPerSide.x, spawnData.MinMaxUnitsPerSide.y + 1);
            for(var i = 0; i < numOfUnits; i++)
            {
                units.Add(CreateRandomUnit(SpawnSide.Right, spawnData));
            }
            return units;
        }

        private UnitModel CreateRandomUnit(SpawnSide side, SpawnData spawnData)
        {
            unitBuilder.Clear();
            var unitPosition = new Vector3();
            var randomX = 0;
            var randomY = 0;
            var tileFound = false;
            while (!tileFound)
            {
                switch (side)
                {
                    case SpawnSide.Left:
                        randomX = Random.Range(0, spawnData.SpawnDistance);
                        randomY = Random.Range(0, mapModel.MapSize.y);
                        break;
                    case SpawnSide.Right:
                        randomX = Random.Range(mapModel.MapSize.x - spawnData.SpawnDistance, mapModel.MapSize.x);
                        randomY = Random.Range(0, mapModel.MapSize.y);
                        break;
                }

                var tile = mapModel.GetTile(randomX, randomY);
                if (!tile.Occupied)
                {
                    unitPosition =  tile.Position;
                    tileFound = true;
                    tile.Occupy();
                }
            }
            var unit = unitBuilder
                .SetBuildRandom()
                .SetSide(side)
                .SetPosition(unitPosition)
                .Build();
            return unit;
        }
    }
}
