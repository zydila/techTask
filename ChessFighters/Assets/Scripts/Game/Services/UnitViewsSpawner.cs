﻿using Game.Models;
using Game.Views;
using UnityEngine;

namespace Game.Services
{
    public class UnitViewsSpawner : MonoBehaviour
    {
        [SerializeField] private Transform root;
        
        public UnitView SpawnUnitView(UnitModel model)
        {
            var unitView = Instantiate<UnitView>(model.UnitData.UnitPrefab, root);
            unitView.transform.position = model.Position;
            if (unitView.Orientation != model.UnitSide)
            {
                var unitTransform = unitView.transform;
                var localScale = unitTransform.localScale;
                unitTransform.localScale = new Vector3(localScale.x * -1, localScale.y, localScale.z);
            }
            unitView.Init(model);
            return unitView;
        }
    }
        
    public enum SpawnSide
    {
        Left = 0,
        Right = 1,
    }
}
