﻿using System;
using Game.Models;

namespace Game.Services
{
    public class UnitMover : IUnitService
    {
        private UnitModel unitModel;

        public bool IsMoving => !moveCompleted;
        private bool moveCompleted = true;

        private Tile targetTile;
        private Action onComplete;

        public void Init(UnitModel unit)
        {
            unitModel = unit;
        }

        public bool Execute(float frameDeltaTime)
        {
            if (moveCompleted)
            {
                return false;
            }

            var delta = targetTile.Position - unitModel.Position;
            var frameDelta = frameDeltaTime * unitModel.UnitData.Speed *
                             (targetTile.Position - unitModel.Position).normalized;
            if (frameDelta.magnitude > delta.magnitude)
            {
                unitModel.SetNewPosition(unitModel.Position + delta);
                moveCompleted = true;
                onComplete?.Invoke();
                onComplete = null;
            }
            else
            {
                unitModel.SetNewPosition(unitModel.Position + frameDelta);
            }

            return true;
        }

        public void OnComplete(Action doOnComplete)
        {
            onComplete = doOnComplete;
        }

        public void SetTargetPosition(Tile targetTile)
        {
            if (targetTile == this.targetTile)
            {
                return;
            }

            this.targetTile = targetTile;
            targetTile.Occupy();
            unitModel.CurrentTile.Free();
            moveCompleted = false;
        }

        public void StopMovement()
        {
            moveCompleted = true;
            onComplete = null;
            if (unitModel.CurrentTile != targetTile)
            {
                targetTile?.Free();
            }
        }
    }
}
