﻿using System;
using System.Collections.Generic;
using Game.Models;
using UnityEngine;

namespace Game.Services
{
    public class GameController : MonoBehaviour, IObserver<GameState>
    {
        private bool processUnits;
        private List<UnitModel> units = new List<UnitModel>();
        
        public void Init(GameModel model)
        {
            model.Subscribe(this);
        }
        
        public void SetUnits(List<UnitModel> units)
        {
            this.units = units;
        }

        public void OnNext(GameState value)
        {
            switch (value)
            {
                case GameState.Prepare:
                    units.Clear();
                    processUnits = false;
                    break;
                case GameState.Finished:
                    processUnits = false;
                    break;
                case GameState.InProcess:
                    processUnits = true;
                    break;
                case GameState.Pause:
                    processUnits = false;
                    break;
            }
        }

        void Update()
        {
            if (!processUnits)
            {
                return;
            }

            foreach (var unit in units)
            {
                unit.ProcessServices(Time.deltaTime);
            }
        }
        
        public void OnCompleted() { }
        public void OnError(Exception error) { }
    }

    public enum GameState
    {
        Prepare,
        InProcess,
        Finished,
        Pause
    }
}
