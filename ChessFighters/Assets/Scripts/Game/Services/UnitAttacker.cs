﻿using System;
using Game.Models;

namespace Game.Services
{
    public class UnitAttacker : IUnitService
    {
        private UnitModel unitModel;
        private UnitModel targetUnit;
        private bool hasTarget;

        private float attackDelayCounter = float.MaxValue;

        private Action onComplete;

        public void Init(UnitModel unit)
        {
            unitModel = unit;
        }

        public void SetTarget(UnitModel target)
        {
            targetUnit = target;
            hasTarget = true;
        }

        public UnitModel GetTargetUnit()
        {
            if (hasTarget)
            {
                return targetUnit;
            }

            return null;
        }

        public void StopAttacking()
        {
            targetUnit = null;
            hasTarget = false;
            onComplete = null;
        }

        public bool Execute(float frameDeltaTime)
        {
            attackDelayCounter += frameDeltaTime;
            unitModel.SetStartAttack(false);
            if (!hasTarget)
            {
                return false;
            }

            if (targetUnit.State == UnitState.Dead)
            {
                onComplete?.Invoke();
                onComplete = null;
                hasTarget = false;
                attackDelayCounter = 0;
                return false;
            }

            if (attackDelayCounter > unitModel.UnitData.AttackSpeed)
            {
                unitModel.SetStartAttack(true);
                targetUnit.ApplyDamage(unitModel.UnitData.Damage);
                hasTarget = false;
                attackDelayCounter = 0;
            }

            return true;
        }

        public void OnComplete(Action doOnComplete)
        {
            onComplete = doOnComplete;
        }
    }
}