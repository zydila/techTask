﻿using Game.Core;
using Game.Models;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Game.Services
{
    //TODO: use DI Container 
    public class Initializer : MonoBehaviour
    {
        [SerializeField] private UnitViewsSpawner viewsSpawner;
        [SerializeField] private GameController gameController;
        [SerializeField] private Tilemap tileMap;

        private readonly GameModel gameModel = DependencyInjector.Resolve<GameModel>();
        private readonly MapModel mapModel = DependencyInjector.Resolve<MapModel>();

        private void Start()
        {
            gameModel.Init(gameController, viewsSpawner);
            mapModel.Init(tileMap);
        }
    }
}
