﻿using UnityEngine;

namespace Game.Models
{
    public class Tile
    {
        public bool Occupied { get; private set; }
        public readonly Vector3 Position;
        public readonly Vector2Int PositionInGrid;

        public Tile(Vector3 position, int x, int y)
        {
            Position = position;
            PositionInGrid = new Vector2Int(x, y);
        }

        public void Occupy()
        {
            Occupied = true;
        }

        public void Free()
        {
            Occupied = false;
        }
    }
}
