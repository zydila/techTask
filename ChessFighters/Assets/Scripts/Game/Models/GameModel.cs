﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Core;
using Game.Services;
using UnityEngine;

namespace Game.Models
{
    //TODO:  I would use UniRx framework instead of observables, so I can subscribe specific fields, and not whole classes 
    public class GameModel : IObservable<GameState> 
    {
        private readonly MapModel mapModel = DependencyInjector.Resolve<MapModel>();
        
        private GameController gameController;
        private UnitViewsSpawner viewsSpawner;
        private readonly UnitSpawner unitSpawner = new UnitSpawner();
        
        public readonly List<UnitModel> AllUnits = new List<UnitModel>();
        private readonly Dictionary<SpawnSide, List<UnitModel>> unitsBySide = new Dictionary<SpawnSide, List<UnitModel>>();
        private readonly List<UnitSubscriber> unitSubscribers = new List<UnitSubscriber>();
        private readonly HashSet<IObserver<GameState>> observers = new HashSet<IObserver<GameState>>();
        
        private GameState gameState;
        public GameState GameState
        {
            get => gameState;
            private set
            {
                if (gameState != value)
                {
                    
                    Debug.Log($"Game state changed: {value}");
                    gameState = value;
                    NotifySubscribersGameStateChanged(gameState);
                }
            }
        }
        
        public void Init(GameController controller, UnitViewsSpawner spawner)
        {
            gameController = controller;
            viewsSpawner = spawner;
            gameController.Init(this);
            unitSpawner.Init();
        }

        private void ClearState()
        {
            AllUnits.ForEach(unit => unit?.ApplyDamage(int.MaxValue));
            AllUnits.Clear();
            unitsBySide.Clear();
            foreach (var unitSubscriber in unitSubscribers)
            {
                unitSubscriber.Dispose();
            }
            unitSubscribers.Clear();
            mapModel.Clear();
            GameState = GameState.Prepare;
        }
        
        public void StartGame()
        {
            ClearState();
            var units = unitSpawner.CreateUnits();
            units[0].SetDebugUnit();
            units.ForEach(unit =>
            {
                viewsSpawner.SpawnUnitView(unit);
                unitSubscribers.Add(new UnitSubscriber(unit, CheckGame));
            });
            AllUnits.AddRange(units);
            gameController.SetUnits(units);
            foreach (var value in Enum.GetValues(typeof(SpawnSide)))
            {
                var side = (SpawnSide) value;
                var unitsSide = units.FindAll(unit => unit.UnitSide == side);
                unitsBySide.Add(side, unitsSide);
            }
            
            GameState = GameState.InProcess;
            units.ForEach(unit => unit.Init());
        }

        private bool CheckFinishCondition()
        {
            var sidesWithAliveUnits = 0;
            foreach (var unitsSide in unitsBySide)
            {
                if (unitsSide.Value.Any(unit => unit.State == UnitState.Alive))
                {
                    sidesWithAliveUnits++;
                }
            }

            return sidesWithAliveUnits <= 1;
        }

        private void CheckGame()
        {
            if (CheckFinishCondition())
            {
                StopGame();
            }
        }

        private void StopGame()
        {
            GameState = GameState.Finished;
        }

        public void SwitchPause()
        {
            switch (GameState)
            {
                case GameState.Pause:
                    GameState = GameState.InProcess;
                    break;
                case GameState.InProcess:
                    GameState = GameState.Pause;
                    break;
            }
        }

        public IDisposable Subscribe(IObserver<GameState> observer)
        {
            observers.Add(observer);
            return null;
        }

        void NotifySubscribersGameStateChanged(GameState newState)
        {
            foreach (var observer in observers)
            {
                observer.OnNext(newState);
            }
        }

        //TODO:  Subscription handler for unit death
        private class UnitSubscriber : IObserver<bool>, IDisposable
        {
            private Action onUnitDied;
            
            public UnitSubscriber(UnitModel unitModel, Action onUnitDied)
            {
                this.onUnitDied = onUnitDied;
                unitModel.Subscribe(this);
            }
            
            public void OnCompleted()
            {
                onUnitDied?.Invoke();
            }

            public void OnError(Exception error) { }
            public void OnNext(bool value) { }

            public void Dispose()
            {
                onUnitDied = null;
            }
        }
    }
}
