﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace Game.Models
{
    public class MapModel
    {
        public Vector2Int MapSize { get; private set; }
        
        private Tile[,] tiles;

        private Tilemap tileMap;
        private BoundsInt cellBounds;

        public void Init(Tilemap tileMap)
        {
            this.tileMap = tileMap;
            cellBounds = tileMap.cellBounds;
            var cellSize = tileMap.cellSize;
            var shift = new Vector3(cellSize.x * cellBounds.position.x, cellSize.x * cellBounds.position.y, 1);

            tiles = new Tile[cellBounds.size.x, cellBounds.size.y];
            for (var i = 0; i < cellBounds.size.x; i++)
            for (var j = 0; j < cellBounds.size.y; j++)
            {
                var position = tileMap.GetCellCenterLocal(new Vector3Int(i, j, 0));
                tiles[i, j] = new Tile(position + shift, i, j);
            }
            MapSize = new Vector2Int(cellBounds.size.x, cellBounds.size.y);
        }

        public Tile GetTile(int x, int y)
        {
            return tiles[x, y];
        }
        

        public Tile GetTile(Vector2Int positionInGrid)
        {
            return tiles[positionInGrid.x, positionInGrid.y];
        }
        
        public Tile GetTileByPosition(Vector3 position)
        {
            var positionInGrid = tileMap.LocalToCell(position);
            return tiles[positionInGrid.x + cellBounds.x * -1, positionInGrid.y + cellBounds.y * -1];
        }

        public void Clear()
        {
            foreach (var tile in tiles)
            {
                tile.Free();
            }
        }

        public Tile FindClosestTileFromToInRange(Tile from, Tile to, int range, bool checkDiagonalTiles)
        {
            var targetPositionInGrid = new Vector2Int(-1, -1);
            var currentSmallestDistance = float.MaxValue;
            for (var i = -range; i <= range; i++)
            for (var j = -range; j <= range; j++)
            {
                if (i == 0 && j == 0)
                {
                    continue;
                }

                if (!checkDiagonalTiles && i != 0 && j != 0)
                {
                    continue;
                }

                if (from.PositionInGrid.x + i >= MapSize.x ||
                    from.PositionInGrid.y + j >= MapSize.y ||
                    from.PositionInGrid.x + i < 0 || 
                    from.PositionInGrid.y + j < 0)
                {
                    continue;
                }

                var checkTile = GetTile(from.PositionInGrid.x + i, from.PositionInGrid.y + j);
                if (checkTile.Occupied)
                {
                    continue;
                }

                var distance = (checkTile.Position - to.Position).magnitude;
                if (distance < currentSmallestDistance)
                {
                    currentSmallestDistance = distance;
                    targetPositionInGrid = checkTile.PositionInGrid;
                }
            }

            return targetPositionInGrid.x == -1 ? from : GetTile(targetPositionInGrid);
        }
    }
}
