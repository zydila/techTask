﻿using System;
using System.Collections.Generic;
using Game.Core;
using Game.Data;
using Game.Services;
using UnityEngine;

namespace Game.Models
{
    //TODO: Use ECS pattern
    public class UnitModel : IObservable<bool>
    {
        public readonly UnitData UnitData;
        
        public int Health { get; private set; }
        public UnitState State { get; private set; }
        public Vector3 Position { get; private set; }
        public SpawnSide UnitSide { get; private set; }
        public Tile CurrentTile { get; private set; }
        public bool DebugUnit { get; private set; }
        public bool StartAttack { get; private set; }

        private readonly UnitMover mover = new UnitMover();
        private readonly UnitAttacker attacker = new UnitAttacker();
        private readonly UnitAI ai = new UnitAI();

        private readonly MapModel mapModel = DependencyInjector.Resolve<MapModel>();
        private HashSet<IObserver<bool>> observers = new HashSet<IObserver<bool>>();

        public UnitModel(UnitData data, SpawnSide side)
        {
            UnitData = data;
            Health = data.Health;
            State = UnitState.Alive;
            UnitSide = side;
        }

        public void SetDebugUnit()
        {
            DebugUnit = true;
        }

        public void Init()
        {
            attacker.Init(this);
            mover.Init(this);
            ai.Init(this);
            ai.SetUnitServices(mover, attacker);
        }

        public UnitModel GetEnemyTarget()
        {
            return DebugUnit ? attacker.GetTargetUnit() : null;
        }
        
        public void ProcessServices(float deltaTime)
        {
            if (State == UnitState.Alive)
            {
                ai.Execute(deltaTime);
                mover.Execute(deltaTime);
                attacker.Execute(deltaTime);
            }
        }

        public void SetNewPosition(Vector3 newPosition)
        {
            Position = newPosition;
            var tile = mapModel.GetTileByPosition(Position);
            if (tile != CurrentTile)
            {
                CurrentTile = tile;
            }
            NotifySubscribers();
        }

        public void SetStartAttack(bool value)
        {
            if (StartAttack == value)
            {
                return;
            }
            StartAttack = value;
            NotifySubscribers();
        }

        public void ApplyDamage(int damage)
        {
            Health -= damage;
            NotifySubscribers();
            if (Health <= 0)
            {
                Health = 0;
                Die();
            }
        }

        private void Die()
        {
            State = UnitState.Dead;
            CurrentTile.Free();
            NotifySubscribers();
            foreach (var observer in observers)
            {
                observer.OnCompleted();
            }
            observers.Clear();
        }

        public IDisposable Subscribe(IObserver<bool> observer)
        {
            observers.Add(observer);
            return null;
        }

        private void NotifySubscribers()
        {
            foreach (var observer in observers)
            {
                observer.OnNext(true);
            }
        }
    }

    public enum UnitState
    {
        Alive,
        Dead
    }
}
