﻿using System;
using Game.Models;
using Game.Services;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views
{
    public class UnitView : MonoBehaviour, IObserver<bool>
    {
        [SerializeField] private SpawnSide orientation;
        
        [SerializeField] private Transform debugTarget;
        [SerializeField] private Transform debugCurrentTile;

        //TODO: I would use single canvas for all healthBars
        [SerializeField] private Transform healthBar;
        [SerializeField] private Image healthAmount;
        
        public SpawnSide Orientation => orientation;
        
        private Vector3 healthBarShift = new Vector3(0, 40, 0); 
        
        private Animator animator;
        private UnitModel unitModel;
        private Camera mainCamera;
        
        public void Init(UnitModel model)
        {
            unitModel = model;
            model.Subscribe(this);
            if (model.UnitSide == SpawnSide.Right)
            {
                GetComponent<SpriteRenderer>().color = Color.red;
            }
            animator = GetComponent<Animator>();
            debugTarget.gameObject.SetActive(model.DebugUnit);
            debugCurrentTile.gameObject.SetActive(model.DebugUnit);
            mainCamera = Camera.main;
            healthBarShift = new Vector3(0, GetComponent<SpriteRenderer>().size.y / 2 + 0.15f, 0);
        }

        private void Update()
        {
            var wantedPos = mainCamera.WorldToScreenPoint(transform.position + healthBarShift);
            healthBar.position = wantedPos;
            if (unitModel.DebugUnit)
            {
                debugCurrentTile.position = unitModel.CurrentTile.Position;
                var target = unitModel.GetEnemyTarget();
                if (target == null)
                {
                    debugTarget.gameObject.SetActive(false);
                }
                else
                {
                    debugTarget.gameObject.SetActive(true);
                    debugTarget.position = unitModel.GetEnemyTarget().Position;
                }
            }
        }

        public void OnCompleted()
        {
            //Die
            Destroy(gameObject);
        }

        public void OnNext(bool value)
        {
            if (value)
            {
                transform.position = unitModel.Position;
                animator.SetBool("enemyAttack", unitModel.StartAttack);
                var healthNormalized = (float) unitModel.Health / unitModel.UnitData.Health;
                healthAmount.fillAmount = healthNormalized;
                if (healthNormalized > 0.66)
                {
                    healthAmount.color = Color.green;
                }
                else if (healthNormalized > 0.33)
                {
                    healthAmount.color = Color.yellow;
                }
                else
                {
                    healthAmount.color = Color.red;
                }
            }
        }
        
        public void OnError(Exception error) { }
    }
}
