﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Views;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "UnitsData", menuName = "ScriptableObjects/UnitsData", order = 1)]
    public class UnitsData : ScriptableObject
    {
        [SerializeField] private List<UnitData> unitsDatas = new List<UnitData>();
         public List<UnitData> UnitsDatas => unitsDatas;

         public bool GetUnitData(string unitId, out UnitData data)
        {
            if (unitsDatas.Any(unit => unit.UnitId == unitId))
            {
                data = unitsDatas.FirstOrDefault(unit => unit.UnitId == unitId);
                return true;
            }
            data = new UnitData();
            return false;
        }
    }

    [Serializable]
    public struct UnitData
    {
        public string UnitId;
        public UnitView UnitPrefab;
        public int Health;
        public int Damage;
        public float Speed;
        public float AttackSpeed;
    }
}
