﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Views;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "GameData", menuName = "ScriptableObjects/SpawnData", order = 1)]
    public class SpawnData : ScriptableObject
    {
        [SerializeField] public Vector2Int MinMaxUnitsPerSide;
        [SerializeField] public int SpawnDistance;
    }
}
