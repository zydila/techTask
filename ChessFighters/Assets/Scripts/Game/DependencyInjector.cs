﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Core
{
//TODO: Simple dependency injector. In real project I would use DI Container, something like Zenject4
    public static class DependencyInjector
    {
        private static readonly HashSet<object> injectedObjects = new HashSet<object>();

        public static T Resolve<T>()
        {
            if (injectedObjects.Any(obj => obj is T))
            {
                return (T) injectedObjects.First(obj => obj is T);
            }

            var instance = Activator.CreateInstance<T>();
            injectedObjects.Add(instance);
            return instance;
        }
    }
}
