﻿using System;
using Game.Core;
using Game.Models;
using Game.Services;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class SimpleGameScreenController : MonoBehaviour, IObserver<GameState>
    {
        [SerializeField] private GameObject StartButton;
        [SerializeField] private Text PauseButton;
        [SerializeField] private Text FinishedText;
    
        private readonly GameModel gameModel = DependencyInjector.Resolve<GameModel>();

        private void Awake()
        {
            gameModel.Subscribe(this);
            OnNext(gameModel.GameState);
        }

        public void OnStartPressed()
        {
            gameModel.StartGame();
        }
        
        public void OnPausePressed()
        {
            gameModel.SwitchPause();
        }

        public void OnNext(GameState value)
        {
            switch (value)
            {
                case GameState.Prepare:
                    StartButton.SetActive(true);
                    PauseButton.transform.parent.gameObject.SetActive(false);
                    FinishedText.gameObject.SetActive(false);
                    break;
                case GameState.InProcess:
                    PauseButton.transform.parent.gameObject.SetActive(true);
                    PauseButton.text = "Pause";
                    StartButton.SetActive(false);
                    FinishedText.gameObject.SetActive(false);
                    break;
                case GameState.Pause:
                    PauseButton.transform.parent.gameObject.SetActive(true);
                    PauseButton.text = "UnPause";
                    StartButton.SetActive(false);
                    FinishedText.gameObject.SetActive(false);
                    break;
                case GameState.Finished:
                    PauseButton.transform.parent.gameObject.SetActive(false);
                    StartButton.SetActive(true);
                    FinishedText.gameObject.SetActive(true);
                    FinishedText.text = "Game Finished";
                    break;
            }
        }
        
        public void OnCompleted() { }
        public void OnError(Exception error) { }
    }
}
