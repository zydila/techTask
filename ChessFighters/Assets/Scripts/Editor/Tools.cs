﻿using Game.Data;
using UnityEditor;

namespace Game.Tools
{
    public static class Tools
    {
        [MenuItem("Tools/Balance/Units Data")]
        public static void OpenBalance()
        {
            EditorUtility.FocusProjectWindow();
            var obj = AssetDatabase.LoadAssetAtPath<UnitsData>("Assets/Resources/Data/UnitsData.asset");
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
        
        [MenuItem("Tools/Balance/Spawn Data")]
        public static void OpenSpawnData()
        {
            EditorUtility.FocusProjectWindow();
            var obj = AssetDatabase.LoadAssetAtPath<SpawnData>("Assets/Resources/Data/SpawnData.asset");
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
    }
}
